function findUniq(arr) {
	var result = false;

	if(!arr.length) {
		return result;
	}
	var sortedArr = [];
	for (var i = 0; i < arr.length; i++) {
		var sorted = arr[i].split('').sort().join('').replace(' ','');
		sortedArr.push(sorted);
	}

	if (sortedArr.length) {
      	var uniqueArr = [];
		for (var i = 0, j = 0, t; i <= sortedArr.length; i++) { 
			if (sortedArr[i] === sortedArr[ i+1 ]) {
				t = sortedArr[i];
			}
			if (sortedArr[i] !== t) {
				uniqueArr[j++] = sortedArr[i];
			}
		}
		var a = uniqueArr.join()
		var indexSorted = sortedArr.indexOf(a);
		result = arr[indexSorted];
	}
	return result;
}

var arr = [ 'abc', 'acb', 'bac', 'test', 'bca', 'cab', 'cba' ]
console.log(findUniq(arr));
